const bcryptjs = require('bcryptjs');
const { User } = require('../model/Users');

const saveUser = async ({email, password, role}) => {
  const user = new User({
    email,
    password: await bcryptjs.hash(password, 10),
    role
  });

  return await user.save();
};

module.exports = {
  saveUser,
};
