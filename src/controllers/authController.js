const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../model/Users');
const { saveUser } = require('../services/authService');

const registerUser = async (req, res, next) => {
  const {email, password, role} = req.body;

  await userJoiSchema.validateAsync({
     email, password, role
  });

  const user = await saveUser({
     email, password, role
  });

  return res.status(200).json({ user, message: 'Success registered!' });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({ message: 'success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
