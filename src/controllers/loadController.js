const {Load, loadJoiSchema} = require("../model/Loads");
const {User} = require("../model/Users");
const {Truck} = require("../model/Truck");
const {getType} = require("../../truckTypes");
const mongoose = require('mongoose');

const getUserLoads = async (req, res) => {
    const findUser = await User.findById(req.user.userId);
    const { status, limit, offset } = req.query;
    if (findUser.role === 'SHIPPER') {
        const loads = await Load.find({ created_by: req.user.userId })
            .limit(+limit)
            .skip(+offset - 1);
        const newLoads = status
            ? loads.filter((item) => item.status === status)
            : loads;
        if (!loads) {
            return res.status(400).json({message: 'Loads not found'});
        }
        res.json({
            loads: newLoads,
        });
    } else {
        if (status) {
            const loads = await Load.find({ created_by: req.user.userId, status })
                .limit(+limit)
                .skip(+offset - 1);
            if (!loads) {
                return res.status(400).json({message: 'Loads not found'});

            }
            res.json({
                loads,
            });
        } else {
            const loads = await Load.find({ assigned_to: req.user.userId })
                .limit(+limit)
                .skip(+offset - 1);
            if (!loads) {
                return res.status(400).json({message: 'Loads not found'});
            }
            res.json({
                loads,
            });
        }
    }

}


const addLoadForUser = async (req, res) => {
    await loadJoiSchema.validateAsync(req.body)

    const findUser = await User.findById(req.user.userId);

    if (findUser.role === 'SHIPPER') {
        const newLoad = new Load({...req.body});
        newLoad.created_by = req.user.userId;

        await newLoad.save().then(() => {
            res.status(200).json({message: 'Load created successfully'})
        });
    } else res.status(400).json({message: 'User is not a shipper'});

};


const getUserActiveLoad = async (req, res) => {
    const findUser = await User.findById(req.user.userId);
    if (findUser.role === 'DRIVER') {
        const findLoad = await Load.findOne({
            assigned_to: req.user.userId,
            status: 'ASSIGNED',
        });
        if (!findLoad) {
            res.status(400).json({message: 'Load not find'});
        } else {
            res.json({
                load: {
                    _id: findLoad._id,
                    created_by: findLoad.created_by,
                    assigned_to: findLoad.assigned_to,
                    status: findLoad.status,
                    state: findLoad.state,
                    name: findLoad.name,
                    payload: findLoad.payload,
                    pickup_address: findLoad.pickup_address,
                    delivery_address: findLoad.delivery_address,
                    dimensions: findLoad.dimensions,
                    logs: findLoad.logs,
                    created_date: findLoad.created_date,
                },
            });
        }
    } else {
        return res.status(400).json({message: 'User is not a driver'});
    }

}


// const getActiveLoadByTruckID = async (truckID) => {
//     return await Load.findOne(
//         {
//             assigned_to: new mongoose.mongo.ObjectId(truckID),
//             status: 'ASSIGNED'
//         },
//         '-__v'
//     )
// }
// const getAssignedTruckByUserID = async (userID) => {
//     return await Truck.findOne({
//         assigned_to: new mongoose.mongo.ObjectId(userID),
//     })
// }
// const updateTruckStatus = async (truckID) => {
//     await Truck.findByIdAndUpdate(truckID, {
//         status: 'IS'
//     })
// }
// const updateUserLoadByID = async (loadID, data) => {
//     return await Load.findByIdAndUpdate(loadID, data)
// }




const iterateToNextLoadState = async (req, res) => {
    const driver = await User.findById(req.user.userId);
    if (driver.role !== 'DRIVER') {
        return res.status(400).json({message: 'User is not a DRIVER'});
    }
    const load = await Load.findOne({
        assigned_to: driver._id,
        status: 'ASSIGNED',
    });
    if (!load) {
        return res.status(400).json({message: 'Load not found'});
    }
    switch (load.state) {
        case 'En route to Pick Up':
            await load.updateOne({
                $set: {
                    state: 'Arrived to Pick Up',
                },
                $push: {
                    logs: {
                        message: `Load state changed to 'Arrived to Pick Up'`,
                        time: new Date().toISOString(),
                    },
                },
            });
            res.json({
                message: "Load state changed to 'Arrived to Pick Up'",
            });
            break;
        case 'Arrived to Pick Up':
            await load.updateOne({
                $set: {
                    state: 'En route to delivery',
                },
                $push: {
                    logs: {
                        message: `Load state changed to 'En route to delivery'`,
                        time: new Date().toISOString(),
                    },
                },
            });
            res.json({
                message: "Load state changed to 'En route to delivery'",
            });
            break;
        case 'En route to delivery':
            await load.updateOne({
                $set: {
                    state: 'Arrived to delivery',
                    status: 'SHIPPED',
                },
                $push: {
                    logs: {
                        message: `Load state changed to 'Arrived to delivery'`,
                        time: new Date().toISOString(),
                    },
                },
            });
            const truck = await Truck.findOne({ shipperId: load.created_by });
            await truck.updateOne({
                $set: {
                    status: 'IS',
                    shipperId: null,
                },
            });
            res.json({
                message: "Load state changed to 'Arrived to delivery'",
            });
            break;
        default:
            break;
    }

}


const getUserLoadById = async (req, res) => {
    const findUser = await User.findById(req.user.userId);

    if (findUser.role === 'SHIPPER') {
        const load = await Load.findById({_id: req.params.id}, '-__v')
        if (load) {
            return res.status(200).json({load});
        }
        res.status(400).json({message: 'Load not found'});
    }
    return res.status(400).json({message: 'User is not a shipper'});
};


const updateUserLoadById = async (req, res) => {
    const findUser = await User.findById(req.user.userId);

    if (findUser.role === 'SHIPPER') {
        const load = await Load.findById({_id: req.params.id}, '-__v');
        if (load) {
            await load.updateOne(req.body);
            return res.status(200).json({message: 'Load update successfully'})
        }
        res.status(400).json({message: 'Load not found'});
    }
    return res.status(400).json({message: 'User is not a shipper'});
}


const deleteUserLoadById = async (req, res) => {
    const findUser = await User.findById(req.user.userId);
    if (findUser.role === 'SHIPPER') {
        const load = await Load.findByIdAndDelete({_id: req.params.id});
        if (!load) {
            return res.status(400).json({message: 'Load not found!'});
        }
        res.status(200).json({message: 'Load deleted successfully'});
    }
    return res.status(400).json({message: 'User is not a shipper'});
}


const postUserLoadById = async (req, res) => {
    const load = await Load.findById(req.params.id);
    if (!load) {
        return res.status(400).json({message: 'Load is not found'});
    }
    await load.updateOne({
        $set: {
            status: 'POSTED',
        },
        $push: {
            logs: {
                message: `Load status changed to 'POSTED'`,
                time: new Date().toISOString(),
            },
        },
    });
    let driver_found = false;
    const truck = await Truck.findOne({
        status: 'IS',
        assigned_to: { $ne: null },
    });

    const truckInfo = truck ? getType(truck.type) : null;
    if (truck) {
        if (
            load.payload < truckInfo.capacity &&
            load.dimensions.width < truckInfo.width &&
            load.dimensions.length < truckInfo.length &&
            load.dimensions.height < truckInfo.height
        ) {
            await truck.updateOne({
                $set: { status: 'OL', shipperId: req.user.userId },
            });
            let driver_found = true;
            await load.updateOne({
                $set: {
                    status: 'ASSIGNED',
                    assigned_to: truck.created_by,
                    state: 'En route to Pick Up',
                },
                $push: {
                    logs: {
                        message: `Load assigned to driver with id ${truck.created_by}`,
                        time: new Date(Date.now()),
                    },
                },
            });
            return res.json({ message: 'Load posted successfully', driver_found });
        }
    } else {
        if (load.status === 'ASSIGNED') {
            return res.status(400).json({message: 'Load is ASSIGNED'});
        }
        await load.updateOne({
            $set: {
                status: 'NEW',
            },
            $push: {
                logs: {
                    message: `Load status changed to 'NEW'! Driver not found!`,
                    time: new Date().toISOString(),
                },
            },
        });
        res.json({
            message: 'Load posted successfully',
            driver_found,
        });
    }

}


const getUserLoadShippingInfoById = async (req, res) => {
    const load = await Load.findById(req.params.id);
    if (!load || load.status !== 'ASSIGNED') {
        return res.status(400).json({message: 'Load not found'});
    }
    const truck = await Truck.findOne({ shipperId: (await load).created_by });
    console.log(truck._id);
    if (!truck) {
        return res.status(400).json({message: 'Truck not found'});
    }
    const { shipperId, ...other } = truck._doc;
    res.json({ load, truck: other });

}

module.exports = {
    getUserLoads,
    addLoadForUser,
    getUserActiveLoad,
    iterateToNextLoadState,
    getUserLoadById,
    updateUserLoadById,
    deleteUserLoadById,
    postUserLoadById,
    getUserLoadShippingInfoById
}