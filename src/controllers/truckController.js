const {Truck, truckJoiSchema} = require('../model/Truck');
const {User} = require("../model/Users");


const getUserTruck = async (req, res) => {
    const {userId} = req.user;
    const findUser = await User.findById(userId);

    if (findUser.role === 'DRIVER') {
        const trucks = await Truck.find({created_by: userId}, '-__v')
        if (!trucks) {
            res.status(400).json({message: 'Truck not find'});
        }
        res.status(200).json({trucks})
    } else res.status(400).json({message: 'User is not a driver'});

}
const addTruckForUser = async (req, res) => {
    const {type} = req.body;
    const {userId} = req.user;
    const findUser = await User.findById(userId);

    if (findUser.role === 'DRIVER') {
        const truck = new Truck({
            userId,
            type
        });
        truck.created_by = userId;

        truck.save().then(() => {
            res.status(200).json({message: 'Truck created successfully'})
        });
    } else res.status(400).json({message: 'User is not a driver'});

};
const getUserTruckById = async (req, res) => {
    const findUser = await User.findById(req.user.userId);

    if (findUser.role === 'DRIVER') {
        const truck = await Truck.findById({_id: req.params.id}, '-__v');

        if (truck) {
            if (findUser._id.toString() !== truck.created_by.toString()) {
                return res.status(400).json({message: 'This is not your truck!'})
            }
            return res.status(200).json({truck});
        }
        res.status(400).json({message: 'Truck not found'});
    }
    return res.status(400).json({message: 'User is not a driver'});

};

const updateUserTruckById = async (req, res) => {
    const findUser = await User.findById(req.user.userId);

    if (findUser.role === 'DRIVER') {
        const truck = await Truck.findById({_id: req.params.id}, '-__v');
        if (truck) {
            if (findUser._id.toString() !== truck.created_by.toString()) {
                return res.status(400).json({message: 'This is not your truck!'})
            }
            await truck.updateOne(req.body);
            return res.status(200).json({message: 'Truck details changed successfully'});
        }
        res.status(400).json({message: 'Truck not found'});
    }
    return res.status(400).json({message: 'User is not a driver'});

}
const deleteUserTruckById = async (req, res) => {
    const findUser = await User.findById(req.user.userId);

    if (findUser.role === 'DRIVER') {
        const truck = await Truck.findByIdAndDelete({_id: req.params.id});
        if (truck) {
            if(findUser._id.toString() !== truck.created_by.toString()) {
                return res.status(400).json({message: 'This is not your truck!'})
            }
            return res.status(200).json({message: 'Truck deleted successfully'})
        }
        return res.status(400).json({message: 'Truck not found'})
    }
    return res.status(400).json({message: 'User is not a driver'});

}
const assignTruckToUserById = async (req, res) => {
    const truck = Truck.findOne({
        _id: req.params.id,
        created_by: req.user.userId,
    });
    if (!truck) {
        return res.status(400).json({message: 'Truck not found'})
    }
    if (truck.status === 'OL') {
        return res.status(400).json({message: 'Truck status is not OL'})
    }
    const findTrucks = await Truck.find({assigned_to: req.user.userId});
    if (findTrucks.length === 1) {
        return res.status(400).json({message: 'Truck length 1'});
    } else {
        await truck.updateOne({$set: {assigned_to: req.user.userId}});
        res.json({
            message: 'Truck assigned successfully',
        });
    }

}

module.exports = {
    getUserTruck,
    addTruckForUser,
    getUserTruckById,
    updateUserTruckById,
    deleteUserTruckById,
    assignTruckToUserById
}