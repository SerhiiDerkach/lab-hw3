const bcryptjs = require('bcryptjs');
const { User } = require('../model/Users');

const getUserProfileInfo = (req, res, next) => {
  User.findById({ _id: req.user.userId }, '-__v -password')
    .then((user) => {
      res.status(200).json({ user });
    });
};

const deleteUserProfile = (req, res, next) => {
  User.findByIdAndDelete({_id: req.user.userId})
      .then(() => {
        res.status(200).json({message: 'Successfully deleted'});
      });
};
const changeUserPassword = async (req, res, next) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById({_id: req.user.userId});
  const match = await bcryptjs.compare(String(oldPassword), String(user.password));
  const hashNewPassword = await bcryptjs.hash(newPassword, 10);

  if (user && match) {
    await User.findByIdAndUpdate(req.user.userId, { password: hashNewPassword});
    return res.status(200).json({message: 'Password updated!'});
  }

  res.status(400).json({message: 'User not found'});
};

module.exports = {
  getUserProfileInfo,
  deleteUserProfile,
  changeUserPassword,
};
