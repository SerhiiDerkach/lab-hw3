const mongoose = require('mongoose');
const Joi = require('joi');

const truckJoiSchema = Joi.object({
    type: Joi.string()
        .alphanum()
        .required(),
    status: Joi.string()
});

const truckSchema = mongoose.Schema({
    created_by: {
        type: mongoose.Schema.Types.ObjectId
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        default: null
    },
    type: {
        type: String,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        default: 'SPRINTER',
        required: true
    },
    status: {
        type: String,
        enum: ['OL', 'IS'],
        default: 'IS'
    },
    shipperId: {
        type: mongoose.Schema.Types.ObjectId
    }
}, {
    timestamps: {
        createdAt: 'createdDate',
        updatedAt: false,
    },
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = {
    Truck,
    truckJoiSchema,
};
