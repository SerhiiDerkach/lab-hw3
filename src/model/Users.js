const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  email: Joi.string()
    .required(),
  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

  role: Joi.string()
    .alphanum(),
});

const userSchema = mongoose.Schema({

  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
}, {
  timestamps: {
    createdAt: 'createdDate',
    updatedAt: false,
  },
});

const User = mongoose.model('user', userSchema);

module.exports = {
  User,
  userJoiSchema,
};
