const express = require('express');
const {asyncWrapper} = require('../../asyncWrapper');
const router = express.Router();
const {authMiddleware} = require("../middleware/authMiddleware");
const {
  getUserLoads, addLoadForUser, getUserActiveLoad,
  iterateToNextLoadState, getUserLoadById,
  updateUserLoadById, deleteUserLoadById, postUserLoadById,
  getUserLoadShippingInfoById,
} = require('../controllers/loadController');

router.get('/',authMiddleware, asyncWrapper(getUserLoads));
router.post('/',authMiddleware, asyncWrapper(addLoadForUser));
router.get('/active',authMiddleware, asyncWrapper(getUserActiveLoad));
router.patch('/active/state',authMiddleware, asyncWrapper(iterateToNextLoadState));
router.get('/:id',authMiddleware, asyncWrapper(getUserLoadById));
router.put('/:id',authMiddleware, asyncWrapper(updateUserLoadById));
router.delete('/:id',authMiddleware, asyncWrapper(deleteUserLoadById));
router.post('/:id/post',authMiddleware, asyncWrapper(postUserLoadById));
router.get('/:id/shipping_info',authMiddleware, asyncWrapper(getUserLoadShippingInfoById));

module.exports = {
  loadRouter: router,
};
