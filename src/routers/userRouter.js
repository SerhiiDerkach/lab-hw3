const express = require('express');

const router = express.Router();
const { getUserProfileInfo, deleteUserProfile, changeUserPassword } = require('../controllers/userController');
const { authMiddleware } = require('../middleware/authMiddleware');
const {asyncWrapper} = require("../../asyncWrapper");



router.get('/me', authMiddleware, asyncWrapper(getUserProfileInfo));
router.delete('/me', authMiddleware, asyncWrapper(deleteUserProfile));
router.patch('/me/password', authMiddleware, asyncWrapper(changeUserPassword));

module.exports = {
  userRouter: router,
};
