const express = require('express');
const router = express.Router();
const {authMiddleware} = require('../middleware/authMiddleware')
const {asyncWrapper} = require("../../asyncWrapper");
const {
  getUserTruck, addTruckForUser, getUserTruckById,
  updateUserTruckById, deleteUserTruckById,
  assignTruckToUserById,
} = require('../controllers/truckController');

router.get('/',authMiddleware, asyncWrapper(getUserTruck));
router.post('/',authMiddleware, asyncWrapper(addTruckForUser));
router.get('/:id',authMiddleware, asyncWrapper(getUserTruckById));
router.put('/:id',authMiddleware, asyncWrapper(updateUserTruckById));
router.delete('/:id',authMiddleware, asyncWrapper(deleteUserTruckById));
router.post('/:id/assign',authMiddleware, asyncWrapper(assignTruckToUserById));

module.exports = {
  truckRouter: router,
};
