const express = require('express');

const router = express.Router();
const { registerUser, loginUser, forgotPassword } = require('../controllers/authController');
const {asyncWrapper} = require("../../asyncWrapper");


router.post('/register',asyncWrapper(registerUser));
router.post('/login', asyncWrapper(loginUser));
// router.post('forgot_password', forgotPassword);

module.exports = {
  authRouter: router,
};
