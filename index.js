const express = require('express');
const morgan = require('morgan');
const app = express();
const dotenv = require('dotenv')
const mongoose = require('mongoose');
const PORT = 8080;
const bdName = 'uber';
dotenv.config();


const { authRouter } = require('./src/routers/authRouter');
const { userRouter } = require('./src/routers/userRouter');
const { truckRouter } = require('./src/routers/truckRouter');
const { loadRouter } = require('./src/routers/loadRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    mongoose.connect(`mongodb+srv://siko:${process.env.PASSWORD}@cluster0.zcwxfvk.mongodb.net/${bdName}?retryWrites=true&w=majority`);
    console.log(`Successfully connected to "${bdName}" database`);
    app.listen(PORT);
    console.log(`Server started at port ${PORT}`)
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  const status = err.status || 500;
  res.status(status).json({ message: err.message });
}
